#version=RHEL7
#perform install
install
# System authorization information
auth --enableshadow --passalgo=sha512
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration AFS and ARCD
firewall --enabled --service=ssh --port=7001:udp --port=4241:tcp
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
#rootpw --iscrypted $1$3pRS3w6k$HTBPZ8mtvf/WCBY9aLb0D/
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog"
# License agreement
eula --agreed
# System timezone 
timezone --utc Europe/Zurich 
# this should not be neeed
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --append="console=ttyS0,115200 console=tty0" --location=mbr --timeout=5 --boot-drive=vda
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --initlabel 
# Disk partitioning information
part / --fstype="xfs" --ondisk=vda --size=3000 --mkfsoptions="-n ftype=1" 
# overlay for XFS/docker: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/7.2_Release_Notes/technology-preview-file_systems.html
# Reboot after installation
reboot

url --url http://linuxsoft.cern.ch/cern/centos/7/os/$basearch/
#repo order is *important* for koji image build target ! 
#repo --name="CentOS-7 - CERN" --baseurl http://linuxsoft.cern.ch/cern/centos/7/cern/$basearch/
#repo --name="CentOS-7 - Updates" --baseurl http://linuxsoft.cern.ch/cern/centos/7/updates/$basearch/
#repo --name="CentOS-7 - Extras" --baseurl http://linuxsoft.cern.ch/cern/centos/7/extras/$basearch/ 

%packages
@cern-base
@cern-openafs-client
chrony
dracut-config-generic
dracut-norescue
firewalld
grub2
iptables-services
kernel
krb5-workstation
redhat-lsb-core
nfs-utils
ntp
rsync
tar
vim-enhanced
yum-utils
yum-plugin-ovl
-aic94xx-firmware
-alsa-firmware
-alsa-lib
-alsa-tools-firmware
-biosdevname
-lcm-firstboot
-iprutils
-ivtv-firmware
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-iwl7260-firmware
-iwl7265-firmware
-libertas-sd8686-firmware
-libertas-sd8787-firmware
-libertas-usb8388-firmware
-linux-firmware
-mcelog
-plymouth
-xorg-x11-font-utils
%end

%post --log=/root/anaconda-post.log

# lock root account
# passwd -d root
# passwd -l root

yum -y update
yum -y install cloud-init cloud-utils-growpart cern-config-users cern-private-cloud-addons NetworkManager-DUID-LLT mesa-libGL locmap
rpm -e kernel-3.10.0-514.el7 linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

# import makowals' ssh key

mkdir -m0700 /root/.ssh/

cat <<EOF >/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA0UTGHsWglyt668nMZj3cejfzexPDAIefeOIxHoOPpAYlgJfW8OzD0SrC/pHQeZYUdX3T3KfSrngI56tzichm1ou+7objoYnCqCJgmqEWbgJe7HZVqLLS0dLClUeHe3OORCZCDjSl8Qw9hYLqt64ORafNkeAL/oNq3CW0sM9p978Nt9ihTVjRK32Kjk9mSL5L5HeIL6RuuvFOy4gJ0Q+bcpg4PZZvNqrZxyT+ghiS90pasbZIYsuiIai1QO5GWDBbpHoDu0q2gIFhh1HBnXS/hu0kNVZ3asswcuyQZ1EqGJqnShf3fzlLQrOG1HkpU5+8bcFhbO5N8p1GXYKkm0JmSQ== makowals@aiadm060.cern.ch
EOF

chmod 0600 /root/.ssh/authorized_keys
restorecon -R /root/.ssh/

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,post}.log
rm -rf /var/cache/yum/*

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end


