# Cloud images

### Image building

note: docker images moved to separate projects (linuxsupport/{cc7-base,slc6-base,slc5-base})

#### How to build:

See ```buildimage.sh``` script.

### Images:

All images are made available in 'IT Linux Support - Images' Openstack.cern.ch Project. (See ```upload2openstack.sh``` script.) 

##### CERN CentOS 7:

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=927884```

##### Scientific Linux CERN 6:

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=927881```

##### Scientific Linux CERN 5:

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=927878```


NOTES:

slc6 -> ksversion RHEL7 <- otherwise kickstart %packages --nocore not acccepted.

slc5 -> ksversion RHEL5 <- otherwise kickstart sections not ending in %end fail.

