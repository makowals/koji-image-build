#version=RHEL6
# System authorization information
authconfig --useshadow --enablemd5 --enablekrb5 --disablenis
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration AFS and ARCD
firewall --enabled --service=ssh --port=7001:udp --port=4241:tcp
# No firstboot
firstboot --disable
# Network information (onboot=yes needed!)
network  --bootproto=dhcp --onboot=yes
# Keyboard layouts
keyboard us
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog"
# RH key not used on SLC
#key --skip
# System timezone
timezone --utc Europe/Zurich
# this should not be neeed
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --append="console=ttyS0,115200 console=tty0"
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --initlabel 
# Disk partitioning information
part / --fstype="ext4" --ondisk=vda --size=3000 # needs to be < 3G for oz img size.
# Reboot after installation
reboot

# installation path, additional repositories
url --url http://linuxsoft.cern.ch/cern/slc69/$basearch/
# repo order is *important* for koji image build target ! 
# temp for 6.9
#repo --name="SLC6 - testing"   --baseurl http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/testing/
repo --name="SLC6 - rhcommon"  --baseurl http://linuxsoft.cern.ch/cern/rhcommon/slc6X/$basearch/RPMS/
repo --name="SLC6 - extras"    --baseurl http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/extras/
repo --name="SLC6 - updates"   --baseurl http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/updates/

%packages
@cern-addons
@quattor-client
@openafs-client
@server-platform
cern-config-users
cloud-init
cern-private-cloud-addons
dracut-modules-growroot
system-config-firewall-base
redhat-lsb-core
tuned
vim-enhanced
yum-plugin-ovl
-efibootmgr
-abrt
-lcm-firstboot
-firstboot
-fprintd
-mcelog
-qt3
-gtk2
-gstreamer-plugins-base
-alsa-utils
-alsa-lib
-abrt
-abrt-libs
-abrt-tui
-CERN-texstyles
-texlive
-qt-x11
-phonon-backend-gstreamer
-redhat-lsb
-redhat-lsb-compat
-redhat-lsb-graphics
-aic94xx-firmware
-atmel-firmware
-bfa-firmware
-iso-codes
-ipw2100-firmware
-ipw2200-firmware
-ivtv-firmware
-iwl1000-firmware
-iwl100-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6050-firmware
-kernel-firmware
-libertas-usb8388-firmware
-netxen-firmware
-ql2100-firmware
-ql2200-firmware
-ql23xx-firmware
-ql2400-firmware
-ql2500-firmware
-rt61pci-firmware
-rt73usb-firmware
-xmlrpc-c-client
-xmlrpc-c
-xml-common
-libreport
-xorg-x11-drv-ati-firmware
-xorg-x11-font-utils
-zd1211-firmware
%end

%post --log=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

# INC1160238
rm -f /var/lib/dbus/machine-id

# use ipv6 and reask for ip .. just in case ..

cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules

if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg
fi

tuned-adm profile virtual-guest

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-sl
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6

# clean up installation - dont , anaconda crashes in post
#rm -f /tmp/{ks-script-*,yum.log}
#rm -f /root/anaconda-{ks,post}.log

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end
