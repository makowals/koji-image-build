#!/bin/bash

#temp script: TBD better.

function usage {
 echo "`basename $0` major test {archs} {date} {rel}"
 echo "         major = 5,6,7" 
 echo "         test = test/prod"
 echo "         arch = x86_64 i686"
 echo "         date = YYYYMMDD"
 echo "         rel = 1"
}

[ -z $1 ] && usage && exit 1
[ -z $2 ] && usage && exit 1

VER=$1
[ -z $3 ] && ARCHS="" || ARCHS=$3
if [ x$2 == "xprod" ]; then
os_edition='Base'
OSEDITION=""
else
os_edition='Test'
OSEDITION="TEST"
fi
[ -z $4 ] && KOJIIMGDATE=`date "+%Y%m%d"` || KOJIIMGDATE=$4
[ -z $5 ] && KOJIIMGREL=1 || KOJIIMGREL=$5

UPLDAYDATE=`echo ${KOJIIMGDATE} | cut -c 1-4 | tr -d '\n' && echo -n "-"`
UPLDAYDATE="${UPLDAYDATE}`echo ${KOJIIMGDATE} | cut -c 5-6 | tr -d '\n' && echo -n \"-\"`"
UPLDAYDATE="${UPLDAYDATE}`echo ${KOJIIMGDATE} | cut -c 7-8 | tr -d '\n'`"
FORMAT="raw"

case $VER in
	5)
	 [ x$ARCHS == "x" ] && ARCHS="i686 x86_64"
	 os_distro="SLC"
	 os_distro_major="5"
	 os_distro_minor="11"
	 IMGPREFIX="slc5-cloud"
	;;
	6)
         [ x$ARCHS == "x" ] && ARCHS="i686 x86_64"
	 os_distro="SLC"
         os_distro_major="6"
         os_distro_minor="9"
	 IMGPREFIX="slc6-cloud"
	;;
	7)
         [ x$ARCHS == "x" ] && ARCHS="x86_64"
	 os_distro="CC"
         os_distro_major="7"
         os_distro_minor="4"
         IMGPREFIX="cc7-cloud"
	;;	
esac


eval `ai-rc 'IT Linux Support - Images'`

for ARCH in ${ARCHS}; do
 daydate=$UPLDAYDATE
 release_date="${daydate}T13:13:13"
 if [ X${OSEDITION} == "X" ]; then
  image_name="$os_distro$os_distro_major - ${ARCH} [$daydate]"
 else
  image_name="$os_distro$os_distro_major ${OSEDITION} - ${ARCH} [$daydate]"
 fi 
 upstream_provider="linux.support@cern.ch"
 [ $ARCH == "i686" ] && FARCH="i386" || FARCH=$ARCH
 img="${IMGPREFIX}-${KOJIIMGDATE}-${KOJIIMGREL}.${FARCH}.${FORMAT}"

 

openstack image create -container-format bare --disk-format ${FORMAT} \
    --property os="LINUX" \
    --property hypervisor_type="qemu" \
    --property os_distro="$os_distro" \
    --property os_distro_major="$os_distro_major" \
    --property os_distro_minor="$os_distro_minor" \
    --property release_date="$release_date" \
    --property os_edition="$os_edition" \
    --property architecture="${ARCH}" \
    --property custom_name="$image_name" \
    --property upstream_provider="$upstream_provider" \
    --property name="$image_name" \
    --file $img 
done    

