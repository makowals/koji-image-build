#!/bin/bash

#temp script: TBD better.

function usage {
 echo "`basename $0` image"
}

[ -z $1 ] && usage && exit 1
IMGFILE=$1
IMGFILEOUT=${IMGFILE/qcow2/raw}

qemu-img convert -f qcow2 -O raw $IMGFILE $IMGFILEOUT 
